#pragma once

// system
#include <cassert>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/reverse/iterator.h>

namespace smeeze {
namespace views {
template<typename container_type>
auto
reverse(container_type && c) {
  auto b(std::begin(c));
  auto e(std::end(c));

  assert(std::begin(c) != std::end(c));

  using iterator_type = smeeze::views::details::reverse::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>;

  return smeeze::views::helpers::make_view(
    iterator_type(std::prev(e), 0),
    iterator_type(e, std::distance(b, e))
  );
}
}
}
