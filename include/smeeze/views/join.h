#pragma once

// system
#include <cassert>
#include <cstdint>
#include <iterator>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/join/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename container_type>
auto
join(container_type && c) {
  assert(std::begin(c) != std::end(c));

  using c_iterator_type = typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type;
  using c_iterator_value = typename smeeze::views::helpers::traits::get_iterator_value<c_iterator_type>::type;
  using c_derived_iterator_type = typename smeeze::views::helpers::traits::get_container_iterator<c_iterator_value>::type;

  auto c_last(smeeze::views::helpers::traits::before_end_iterator(c));

  using iterator_type = smeeze::views::details::join::iterator<c_iterator_type, c_derived_iterator_type>;

  return smeeze::views::helpers::make_view(
    iterator_type(std::begin(c), c_last, std::begin(*std::begin(c)), std::end(*std::begin(c))),
    iterator_type(c_last, std::end(*c_last))
  );
}
}
}
