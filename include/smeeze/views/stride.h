#pragma once

// system
#include <cassert>
#include <cstdint>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/stride/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename container_type>
auto
stride(container_type && c, size_t stride) {
  assert(std::distance(std::begin(c), std::end(c)) % stride == 0);

  using iterator_type = smeeze::views::details::stride::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>;

  return smeeze::views::helpers::make_view(
    iterator_type(std::begin(c), stride),
    iterator_type(std::end(c), 0)
  );
}
}
}
