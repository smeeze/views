#pragma once

// system
#include <cassert>
#include <cstdint>
#include <iterator>
#include <tuple>
// smeeze
#include <smeeze/views/details/join2/capture.h>
#include <smeeze/views/details/join2/iterator.h>
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename... Ts>
auto
join2(Ts && ... ts) {
  using capture_tuple_type = std::tuple<smeeze::views::details::join2::capture<Ts>...>;

  capture_tuple_type ct(std::forward<Ts>(ts)...);

  size_t n(0);
  smeeze::views::helpers::traits::for_each([&n](const auto & t) {
    std::cout << "-----" << t.size() << std::endl;
    n += t.size();
  }, ct);

  return std::vector<int>{13, 13, 13};
}
}
}
