#pragma once

// smeeze
#include <smeeze/views/details/iota/iterator.h>
#include <smeeze/views/helpers/generic_view.h>

namespace smeeze {
namespace views {
template<typename T>
auto
iota(size_t count, const T & start = 0, const T & increment = 1) {
  using iterator = smeeze::views::details::iota::iterator<T>;

  return smeeze::views::helpers::make_view(
    iterator(0, start, increment),
    iterator(count, T(count) * increment + start, increment)
  );
}
}
}
