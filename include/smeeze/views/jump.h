#pragma once

// system
#include <cassert>
#include <cstdint>
#include <iterator>
// smeeze
#include <smeeze/views/details/jump/iterator.h>
#include <smeeze/views/details/jump/table.h>
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {

template<typename container_type>
auto
jump(container_type && c, const smeeze::views::details::jump::table & j) {
  assert(std::begin(c) != std::end(c));

  using iterator_type = typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type;

  auto b(std::begin(c));
  auto e(std::next(b, j.get_last_offset()));
  return smeeze::views::helpers::make_view(
    smeeze::views::details::jump::iterator<iterator_type>(b, j),
    smeeze::views::details::jump::iterator<iterator_type>(e, j.size())
  );
}

template<typename container_type, typename indices_table_type>
auto
jump(container_type && c, const indices_table_type & i) {
  return jump(c, smeeze::views::details::jump::table(i));
}
}
}
