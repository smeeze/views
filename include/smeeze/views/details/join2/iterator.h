#pragma once

// system
#include <algorithm>
#include <iterator>
#include <tuple>
// smeeze
#include <smeeze/views/helpers/misc.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace join2 {
template<typename I, typename II>
class iterator : public std::iterator<std::forward_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<II>::type> {
public:
  iterator(const I & oi, const I & oe, const II & ii, const II & ie) : _outer_iter(oi), _outer_end(oe), _inner_iter(ii), _inner_end(ie) {
  }
  iterator(const I & oi, const II & ii) : iterator(oi, oi, ii, ii) {
  }
  // prefix
  iterator & operator++() {
    assert(_inner_iter != _inner_end);
    ++_inner_iter;
    if(_inner_iter == _inner_end && _outer_iter != _outer_end) {
      ++_outer_iter;
      _inner_iter = std::begin(*_outer_iter);
      _inner_end = std::end(*_outer_iter);
    }
    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _outer_iter == rhs._outer_iter && _inner_iter == rhs._inner_iter; }
  bool operator!=(const iterator & rhs) const { return _outer_iter != rhs._outer_iter || _inner_iter != rhs._inner_iter; }

  // dereference
  const auto & operator*() const { return *_inner_iter; }
        auto & operator*()       { return *_inner_iter; }
private:
  I  _outer_iter;
  I  _outer_end;
  II _inner_iter;
  II _inner_end;
};
}
}
}
}
