#pragma once

// system
#include <iostream>

namespace smeeze {
namespace views {
namespace details {
namespace join2 {
template<typename T>
class capture {
public:
  capture(T &&) {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
  }

  size_t size() const {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return 0;
  }
};
}
}
}
}
