#pragma once

// system
#include <iterator>
// smeeze
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace zip {
template<typename... iterator_list>
class iterator {
public:
  using tuple_type = std::tuple<typename smeeze::views::helpers::traits::get_iterator_reference<iterator_list>::type...>;

  iterator(const iterator_list & ... il) : _iterator_list(il...) { }

  // compare
  bool operator==(const iterator & other) const {
    return _compare_equal(_iterator_list, other._iterator_list);
  }
  bool operator!=(const iterator & other) const {
    return !operator==(other);
  }

  // advance
  iterator & operator++() {
    _advance(typename smeeze::views::helpers::traits::generator<sizeof...(iterator_list)>::type());
    return *this;
  }

  // dereference
  tuple_type operator*() {
    return _dereference(typename smeeze::views::helpers::traits::generator<sizeof...(iterator_list)>::type());
  }
private:
  // compare
  template<size_t N, typename T>
  struct _compare_equal_helper {
    static bool Compare(const T & l, const T & r) {
      if(std::get<N>(l) == std::get<N>(r)) {
        return true;
      }
      return _compare_equal_helper<N - 1, T>::Compare(l, r);
    }
  };

  template<typename T>
  struct _compare_equal_helper<0, T> {
    static bool Compare(const T & l, const T & r) {
      return std::get<0>(l) == std::get<0>(r);
    }
  };

  template<typename head, typename... tail>
  bool _compare_equal(const std::tuple<head, tail...> & l, const std::tuple<head, tail...> & r) const {
   return _compare_equal_helper<sizeof...(tail), std::tuple<head, tail...>>::Compare(l, r);
  }

  // advance
  template<typename... T>
  inline void _place_holder(T && ...) {}

  template<size_t... S>
  void _advance(smeeze::views::helpers::traits::sequence<S...>) {
    _place_holder(++ std::get<S>(_iterator_list)...);
  }

  // dereference
  template<size_t... S>
  tuple_type _dereference(smeeze::views::helpers::traits::sequence<S...>) {
     return tuple_type(*std::get<S>(_iterator_list)...);
  }
private:
  std::tuple<iterator_list...> _iterator_list;
};
}
}
}
}
