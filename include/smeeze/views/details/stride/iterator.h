#pragma once

// system
#include <cstdint>
#include <iterator>
// smeeze
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace stride {
template<typename reference_iterator>
class iterator : public std::iterator<std::forward_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<reference_iterator>::type> {
public:
  iterator(reference_iterator ri, size_t stride) : _ri(ri), _stride(stride) { }

  // prefix
  iterator & operator++() {
    std::advance(_ri, _stride);
    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _ri == rhs._ri; }
  bool operator!=(const iterator & rhs) const { return _ri != rhs._ri; }

  // dereference
  const auto & operator*() const { return *_ri; }
        auto & operator*()       { return *_ri; }
private:
  reference_iterator _ri;
  size_t             _stride;
};
}
}
}
}
