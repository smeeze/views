#pragma once

namespace smeeze {
namespace views {
namespace details {
namespace jump {
class table {
public:
  table() = default;

  template<typename C>
  table(const C & c) {
    _jumps.reserve(c.size());

    _last_offset = 0;
    for(const auto & i : c) {
      _jumps.push_back(i - _last_offset);
      _last_offset = i;
    }
  }

  auto cbegin() const { return std::begin(_jumps); }
  auto cend()   const { return std::end(_jumps); }

  auto begin() const { return std::begin(_jumps); }
  auto end()   const { return std::end(_jumps); }

  const auto & get_last_offset() const { return _last_offset; }

  auto size() const { return _jumps.size(); }
private:
  std::vector<int> _jumps;
  int              _last_offset;
};
}
}
}
}
