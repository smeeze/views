#pragma once

// system
#include <algorithm>
#include <iterator>
// smeeze
#include <smeeze/views/details/jump/table.h>
#include <smeeze/views/helpers/misc.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace jump {
template<typename I>
class iterator : public std::iterator<std::forward_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  iterator(const I & i, const smeeze::views::details::jump::table & jt) : _iterator(i), _jump_table(jt), _jump_table_iterator(std::begin(jt)), _count(0) {
    _next();
  }
  iterator(const I & i, int count) : _iterator(i), _count(count) {
  }
  // prefix
  iterator & operator++() {
    _next();
    ++_count;

    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _count == rhs._count; }
  bool operator!=(const iterator & rhs) const { return _count != rhs._count; }

  // dereference
  const auto & operator*() const { return *_iterator; }
        auto & operator*()       { return *_iterator; }
private:
  void _next() {
    std::advance(_iterator, *_jump_table_iterator);
    ++_jump_table_iterator;
  }
private:
  I                                                                                                 _iterator;
  smeeze::views::details::jump::table                                                               _jump_table;
  smeeze::views::helpers::traits::get_container_iterator<smeeze::views::details::jump::table>::type _jump_table_iterator;
  int                                                                                               _count;
};
}
}
}
}
