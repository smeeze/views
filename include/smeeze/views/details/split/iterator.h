#pragma once

// system
#include <cassert>
#include <algorithm>
#include <iterator>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/misc.h>

namespace smeeze {
namespace views {
namespace details {
namespace split {
template<typename I, typename P>
class iterator : public std::iterator<std::forward_iterator_tag, smeeze::views::helpers::generic_view<I>> {
public:
  iterator(const I & b, const I & e, const P & p) : _view(b, b), _end(e), _unary_predicate(p) {
    _find_next_state();
  }
  iterator(const I & b, const P & p) : _view(b, b), _unary_predicate(p) {
  }

  // prefix
  iterator & operator++() {
    _find_next_state();
    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return std::begin(_view) == std::begin(rhs._view); }
  bool operator!=(const iterator & rhs) const { return std::begin(_view) != std::begin(rhs._view); }

  // dereference
  const auto & operator*() const { return _view; }
        auto & operator*()       { return _view; }
private:
  void _find_next_state() {
    auto b(std::find_if_not(_view.end(), _end, _unary_predicate));
    auto e(std::find_if    (b,           _end, _unary_predicate));
    _view = smeeze::views::helpers::generic_view<I>(b, e);
  }
private:
  smeeze::views::helpers::generic_view<I> _view;
  I                                       _end;
  P                                       _unary_predicate;
};
}
}
}
}
