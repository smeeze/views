#pragma once

// system
#include <iterator>
// smeeze
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace rotate {
template<typename I>
class iterator : public std::iterator<std::bidirectional_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  iterator(const I & i, int offset, int shift, int size) : _iter(i), _offset(offset), _shift(shift), _size(size), _index(0) {
    _set_iter(_offset + _shift);
  }

  // prefix
  auto & operator--() { _decrement(1); return *this; }
  auto & operator++() { _increment(1); return *this; }

  auto & operator-=(int n) { _decrement(n); return * this; }
  auto & operator+=(int n) { _increment(n); return * this; }

  // compare
  bool operator==(const iterator & rhs) const { return _offset == rhs._offset; }
  bool operator!=(const iterator & rhs) const { return _offset != rhs._offset; }

  // dereference
  const auto & operator*() const { return *_iter; }
        auto & operator*()       { return *_iter; }
private:
  void _increment(int n) {
    assert(n>0);
    _offset += n;

    _set_iter(_offset + _shift);
  }
  void _decrement(int n) {
    assert(n>0);

    _offset -= n;

    _set_iter(_offset + _shift);
  }
  void _set_iter(int new_index) {
    if(new_index >= _size) { new_index -= _size; }
    if(new_index <  0)     { new_index += _size; }

    std::advance(_iter, new_index - _index);

    _index = new_index;
  }
private:
  I   _iter;
  int _offset;
  int _shift;
  int _size;
  int _index;
};
}
}
}
}
