#pragma once

// system
#include <iterator>
// smeeze
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace reverse {
template<typename I>
class iterator : public std::iterator<std::bidirectional_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  iterator(const I & i, size_t offset) : _iter(i), _offset(offset) {
  }

  // prefix
  auto & operator--() {
    ++_iter;
    --_offset;
    return *this;
  }
  auto & operator++() {
    --_iter;
    ++_offset;
    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _offset == rhs._offset; }
  bool operator!=(const iterator & rhs) const { return _offset != rhs._offset; }

  // dereference
  const auto & operator*() const { return *_iter; }
private:
  I      _iter;
  size_t _offset;
};
}
}
}
}
