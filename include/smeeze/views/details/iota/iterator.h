#pragma once

// system
#include <iterator>

namespace smeeze {
namespace views {
namespace details {
namespace iota {
template<typename T>
class iterator : public std::iterator<std::bidirectional_iterator_tag, T> {
public:
  iterator(size_t offset, const T & value, const T & increment) : _offset(offset), _value(value), _increment(increment) { }

  // prefix
  iterator & operator--() {
    --_offset;
    _value -= _increment;
    return *this;
  }
  iterator & operator++() {
    ++_offset;
    _value += _increment;
    return *this;
  }
  auto & operator-=(int n) {
    _offset -= n;
    _value -= T(n) * _increment;
    return * this;
  }
  auto & operator+=(int n) {
    _offset += n;
    _value += T(n) * _increment;
    return * this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _offset == rhs._offset; }
  bool operator!=(const iterator & rhs) const { return _offset != rhs._offset; }

  // dereference
  const T & operator*() const { return _value; }
private:
  size_t _offset;
  T      _value;
  T      _increment;
};
}
}
}
}
