#pragma once

// system
#include <cassert>
#include <algorithm>
#include <iterator>
// smeeze
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace strip {
template<typename I, typename P>
class iterator : public std::iterator<std::forward_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  iterator(const I & b, const I & e, P && p) : _begin(b), _end(e), _unary_predicate(std::forward<P>(p)) {
    _find_valid_state();
  }
  iterator(const I & b, P && p) : _begin(b), _unary_predicate(std::forward<P>(p)) {
  }

  // prefix
  iterator & operator++() {
    ++_begin;
    _find_valid_state();
    return *this;
  }

  // compare
  bool operator==(const iterator & rhs) const { return _begin == rhs._begin; }
  bool operator!=(const iterator & rhs) const { return _begin != rhs._begin; }

  // dereference
  const auto & operator*() const { return *_begin; }
        auto & operator*()       { return *_begin; }
private:
  void _find_valid_state() {
    _begin = std::find_if_not(_begin, _end, _unary_predicate);
  }
private:
  I _begin;
  I _end;
  P _unary_predicate;
};
}
}
}
}
