#pragma once

// system
#include <iostream>
#include <iterator>
// smeeze
#include <smeeze/views/details/xy/delegate/iterator.h>
#include <smeeze/views/details/xy/delegate/offset.h>
#include <smeeze/views/details/xy/helper.h>
#include <smeeze/views/details/xy/view.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
template<typename I>
class xy_iterator : public std::iterator<std::bidirectional_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  // constructor
  xy_iterator(const I & i,
              size_t offset,
              bool row_major,
              size_t major_width,
              size_t x,
              size_t y,
              size_t minor_width,
              size_t minor_height) : _iter(i),
                                     _offset(offset),
                                     _row_major(row_major),
                                     _major_width(major_width),
                                     _x(x),
                                     _y(y),
                                     _minor_width(minor_width),
                                     _minor_height(minor_height) {
  }

  // advance
  auto & operator--() {
    _jump_backward(1);
    return * this;
  }
  auto & operator++() {
    _jump_forward(1);
    return * this;
  }

  auto operator-(int n) const { assert(n>0); return xy_iterator(*this)-=n; }
  auto operator+(int n) const { assert(n>0); return xy_iterator(*this)+=n; }

  auto & operator-=(int n) { assert(n>0); _jump_backward(n); return * this; }
  auto & operator+=(int n) { assert(n>0); _jump_forward(n); return * this; }

  // compare
  bool operator==(const xy_iterator & rhs) const { return _offset == rhs._offset; }
  bool operator!=(const xy_iterator & rhs) const { return _offset != rhs._offset; }

  // deref
        auto & operator*()       { return * _iter; }
  const auto & operator*() const { return * _iter; }

  auto operator-(const xy_iterator & rhs) const { return _offset - rhs._offset; }

  auto operator<(const xy_iterator & rhs) const { return _offset < rhs._offset; }
private:
  void _jump_backward(int count) {
    assert(count > 0);

    TODO;
  }
  void _jump_forward(int count) {
    assert(count > 0);

    TODO;
  }
  void _set_iterator(int new_major_offset) {
  }
private:
  I      _iter;
  size_t _major_offset;
  size_t _offset;
  bool   _row_major;
  size_t _major_width;
  size_t _x;
  size_t _y;
  size_t _minor_width;
  size_t _minor_height;
};
}
}
}
}
