#pragma once

// system
#include <iostream>
#include <iterator>
// smeeze
#include <smeeze/views/details/xy/delegate/iterator.h>
#include <smeeze/views/details/xy/delegate/offset.h>
#include <smeeze/views/details/xy/helper.h>
#include <smeeze/views/details/xy/view.h>
#include <smeeze/views/helpers/misc.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
template<typename Holder, typename I>
class jump_iterator : public std::iterator<std::bidirectional_iterator_tag, typename smeeze::views::helpers::traits::get_iterator_value<I>::type> {
public:
  // constructor
  jump_iterator(const I & i, size_t offset, bool row_major, size_t major_width, size_t x, size_t y, size_t minor_width, size_t minor_height) :
    _holder(i, smeeze::views::details::xy::helper::minor_to_major_offset(offset, row_major, x, y, minor_width, minor_height, major_width)),
    _offset(offset),
    _jump_test(0) {
    if(row_major) {
      _jump_index = minor_width;
      _stride1    = 1;
      _stride2    = (1 + major_width - minor_width);
    } else {
      _jump_index = minor_height;
      _stride1    = major_width;
      _stride2    = 1 - (major_width * (minor_height - 1));
    }
  }

  // advance
  auto & operator--() {
    _jump_backward();
    return * this;
  }
  auto & operator++() {
    _jump_forward();
    return * this;
  }
  auto & operator++(int n) {
    _jump_forward(n);
    return *this;
  }
  auto operator-(int n) const { assert(n>0); return jump_iterator(*this)-=n; }
  auto operator+(int n) const { assert(n>0); return jump_iterator(*this)+=n; }

  auto & operator-=(int n) { assert(n>0); _jump_backward(n); return * this; }
  auto & operator+=(int n) { assert(n>0); _jump_forward(n); return * this; }

  // compare
  bool operator==(const jump_iterator & rhs) const { return _offset == rhs._offset; }
  bool operator!=(const jump_iterator & rhs) const { return _offset != rhs._offset; }

  // deref
        auto & operator*()       { return * _holder; }
  const auto & operator*() const { return * _holder; }

  auto operator-(const jump_iterator & rhs) const { return _holder - rhs._holder; }

  auto operator<(const jump_iterator & rhs) const { return _holder < rhs._holder; }
private:
  //
  //
  //
  void _jump(int jump) {
    _holder += jump;
  }
  //
  //
  //
  void _jump_backward() {
    if(_jump_test == 0) {
      _jump(-_stride2);
      _jump_test = _jump_index;
    } else {
      _jump(-_stride1);
    }

    --_offset;
    --_jump_test;
  }
  //
  //
  //
  void _jump_backward(int count) {
    assert(count > 0);
    int jump(0);
    while(count--) {
      if(_jump_test == 0) {
        jump -= _stride2;
        _jump_test = _jump_index;
      } else {
        jump -= _stride1;
      }

      --_offset;
      --_jump_test;
    }
    _jump(jump);
  }
  //
  //
  //
  void _jump_forward() {
    ++_offset;
    ++_jump_test;

    if(_jump_test < _jump_index) {
      _jump(_stride1);
    } else {
      _jump(_stride2);
      _jump_test = 0;
    }
  }
  //
  //
  //
  void _jump_forward(int count) {
    assert(count > 0);
    int jump(0);
    while(count--) {
      ++_offset;
      ++_jump_test;

      if(_jump_test < _jump_index) {
        jump += _stride1;
      } else {
        jump += _stride2;
        _jump_test = 0;
      }
    }
    _jump(jump);
  }
private:
  Holder _holder;
  int    _offset;
  int    _jump_test;
  int    _jump_index;
  int    _stride1;
  int    _stride2;
};
}
}
}
}
