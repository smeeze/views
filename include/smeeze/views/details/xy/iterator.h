#pragma once

// system
#include <iostream>
#include <iterator>
// smeeze
#include <smeeze/views/details/xy/jump_iterator.h>
#include <smeeze/views/details/xy/xy_iterator.h>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
template<typename I>
using iterator = smeeze::views::details::xy::jump_iterator<smeeze::views::details::xy::delegate::iterator<I>, I>;
// using iterator = smeeze::views::details::xy::jump_iterator<smeeze::views::details::xy::delegate::offset<I>, I>;
// using iterator = smeeze::views::details::xy::xy_iterator<I>;
}
}
}
}
