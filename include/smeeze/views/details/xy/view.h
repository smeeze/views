#pragma once

// smeeze
#include <smeeze/views/details/xy/iterator.h>
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/misc.h>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
template<typename I>
class view : public smeeze::views::helpers::generic_view<I> {
public:
  view(I && b, I && e, size_t major_width, size_t major_height) : smeeze::views::helpers::generic_view<I>(std::forward<I>(b), std::forward<I>(e)), _width(major_width), _height(major_height) {
  }
///////////////////////////////////////////////////////////////////////////////
  const size_t & get_width() const { return _width; }
  const size_t & get_height() const { return _height; }
///////////////////////////////////////////////////////////////////////////////
  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_view(size_t x, size_t y, size_t w, size_t h) const {
    return view<bidir>(
      bidir(this->begin(), 0,     true, get_width(), x, y, w, h),
      bidir(this->begin(), w * h, true, get_width(), x, y, w, h),
      w,
      h
    );
  }

  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_view() const {
    return get_view(0, 0, get_width(), get_height());
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_view_transposed(size_t x, size_t y, size_t w, size_t h) const {
    return view<bidir>(
      bidir(this->begin(), 0,     false, get_width(), x, y, w, h),
      bidir(this->begin(), w * h, false, get_width(), x, y, w, h),
      h,
      w
    );
  }

  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_view_transposed() const {
    return get_view_transposed(0, 0, get_width(), get_height());
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_row(size_t n) const {
    return get_view(0, n, get_width(), 1);
  }

  template<typename bidir = smeeze::views::details::xy::iterator<I>>
  view<bidir>
  get_column(size_t n) const {
    return get_view(n, 0, 1, get_height());
  }
private:
  size_t _width;
  size_t _height;
};
}
}
}
}
