#pragma once

// system
#include <cassert>
#include <algorithm>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
namespace delegate {
template<typename I>
class iterator {
public:
  iterator(const I & i, int offset) : _iterator(std::next(i, offset)) { }

  auto & operator-=(int i) { if(i>0) { _iterator -= i; } else { _iterator += -i; } return *this; }
  auto & operator+=(int i) { if(i>0) { _iterator += i; } else { _iterator -= -i; } return *this; }

        auto & operator*()       { return * _iterator; }
  const auto & operator*() const { return * _iterator; }

  auto operator-(const iterator & rhs) const { return _iterator - rhs._iterator; }
  auto operator+(const iterator & rhs) const { return _iterator + rhs._iterator; }

  auto operator<(const iterator & rhs) const { return _iterator < rhs._iterator; }
private:
  I _iterator;
};
}
}
}
}
}
