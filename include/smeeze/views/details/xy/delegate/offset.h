#pragma once

// system
#include <cassert>
#include <algorithm>

namespace smeeze {
namespace views {
namespace details {
namespace xy {
namespace delegate {
template<typename I>
class offset {
public:
  offset(const I & i, int offset) : _iterator(i), _offset(offset) { }

  const auto & operator-=(int i) { _offset -= i; return * this; }
  const auto & operator+=(int i) { _offset += i; return * this; }

        auto & operator*()       { return * std::next(_iterator, _offset); }
  const auto & operator*() const { return * std::next(_iterator, _offset); }

  auto operator-(const offset & rhs) const { return _offset - rhs._offset; }
  auto operator+(const offset & rhs) const { return _offset + rhs._offset; }

  auto operator<(const offset & rhs) const { return _offset < rhs._offset; }

  offset & operator=(const offset & rhs) {
    // hack
    const_cast<I &>(_iterator) = rhs._iterator;
    _offset = rhs._offset;
    return *this;
  }
private:
  const I _iterator;
  int     _offset;
};
}
}
}
}
}
