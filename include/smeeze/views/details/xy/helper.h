#pragma once

namespace smeeze {
namespace views {
namespace details {
namespace xy {
namespace helper {
///////////////////////////////////////////////////////////////////////////////
inline
int
minor_to_major_offset(int offset, bool row_major, size_t minor_x, size_t minor_y, size_t minor_width, size_t minor_height, size_t major_width) {
  size_t min_x;
  size_t min_y;

  if(row_major) {
    min_y = offset / minor_width;
    min_x = offset - min_y  * minor_width;
  } else {
    min_x = offset / minor_height;
    min_y = offset - min_x * minor_height;
  }
  auto maj_x(minor_x + min_x);
  auto maj_y(minor_y + min_y);

  return maj_y * major_width + maj_x;
}
}
}
}
}
}
