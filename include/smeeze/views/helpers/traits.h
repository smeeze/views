#pragma once

// system
#include <cassert>
#include <iterator>
#include <tuple>
#include <type_traits>

namespace smeeze {
namespace views {
namespace helpers {
namespace traits {
///////////////////////////////////////////////////////////////////////////////
template<typename I>
struct get_iterator_reference {
  using type =
    decltype(
      *std::declval<I>()
    )
  ;
};
///////////////////////////////////////////////////////////////////////////////
template<typename I>
struct get_iterator_value {
  using type =
    typename std::remove_reference<
      typename get_iterator_reference<I>::type
    >::type;
};
///////////////////////////////////////////////////////////////////////////////
template<typename T>
struct get_container_iterator {
  using type =
    typename std::remove_cv<
      typename std::remove_reference<
        decltype(
          std::begin(
            std::declval<T>()
          )
        )
      >::type
    >::type
  ;
};
///////////////////////////////////////////////////////////////////////////////
template<size_t...>
struct sequence { };

template<size_t N, size_t... S>
struct generator : generator<N-1, N-1, S...> { };

template<size_t... S>
struct generator<0, S...> {
  using type = sequence<S...>;
};
///////////////////////////////////////////////////////////////////////////////
template<std::size_t I = 0, typename F, typename... Tp>
typename std::enable_if<I == sizeof...(Tp), void>::type
for_each(const F &, const std::tuple<Tp...> &) {
}

template<std::size_t I = 0, typename F, typename... Tp>
typename std::enable_if<I < sizeof...(Tp), void>::type
for_each(const F & f, const std::tuple<Tp...> & t) {
  // std::cout << __PRETTY_FUNCTION__ << "(" << std::get<I>(t) << ")" << std::endl;
  f(std::get<I>(t));

  for_each<I + 1, F, Tp...>(f, t);
}
///////////////////////////////////////////////////////////////////////////////
template<typename C>
auto
before_end_iterator(C && c) {
  const auto & i(std::begin(c));
  const auto & e(std::end  (c));
  assert(i != e);
  auto d(std::distance(i, e));
  return std::next(i, d-1);
};
}
}
}
}
