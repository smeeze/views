#pragma once

// system
#include <utility>

namespace smeeze {
namespace views {
namespace helpers {
template<typename iterator_type>
class generic_view {
public:
  generic_view(const iterator_type & b, const iterator_type & e) : _begin(b), _end(e) {
  }

  const iterator_type & cbegin() const { return _begin; }
  const iterator_type & cend()   const { return _end; }

  const iterator_type & begin() const { return _begin; }
  const iterator_type & end()   const { return _end; }
private:
  iterator_type _begin;
  iterator_type _end;
};

template<typename iterator_type>
generic_view<iterator_type>
make_view(iterator_type && b, iterator_type && e) {
  return generic_view<iterator_type>(std::forward<iterator_type>(b),
                                     std::forward<iterator_type>(e));
}
}
}
}
