#pragma once

// system
#include <iostream>
#include <iomanip>

#define TODO { std::cerr << __FILE__ << "(" << __LINE__ << ")" << std::endl; std::terminate(); }
