#pragma once

// system
#include <ostream>

namespace smeeze {
namespace views {
namespace helpers {
namespace details {
template<typename T>
class print {
public:
  print(const T & t) : _t(t) { }
  auto begin() const { return std::begin(_t); }
  auto end() const { return std::end(_t); }
private:
  const T & _t;
};
}

template<typename T>
auto
print(const T & t) {
  return details::print<T>(t);
}
}
}
}

template<typename T>
inline
std::ostream &
operator<<(std::ostream & out, const smeeze::views::helpers::details::print<T> & p) {
  out << "[";
  for(const auto & v: p) {
    out << " " << v;
  }
  out << " ]";
  return out;
}
