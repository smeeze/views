#pragma once

// system
#include <cassert>
#include <cstdint>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/rotate/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename container_type>
auto
rotate(container_type && c, int shift = 0) {
  using iterator_type = smeeze::views::details::rotate::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>;

  auto b(std::begin(c));
  auto e(std::end(c));
  int size(std::distance(b, e));
  return smeeze::views::helpers::make_view(
    iterator_type(b, 0,    shift, size),
    iterator_type(b, size, shift, size)
  );
}
}
}
