#pragma once

// views
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/split/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename container_type, typename unary_predicate>
auto
split(container_type && c, unary_predicate && p) {
  using iterator_type = smeeze::views::details::split::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type, unary_predicate>;

  auto b(std::begin(c));
  auto e(std::end(c));
  return smeeze::views::helpers::make_view(
    iterator_type(b, e, std::forward<unary_predicate>(p)),
    iterator_type(e, std::forward<unary_predicate>(p))
  );
}
}
}
