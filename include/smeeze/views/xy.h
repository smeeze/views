#pragma once

// system
#include <cassert>
#include <cstdint>
#include <iomanip>
#include <ostream>
// smeeze
#include <smeeze/views/details/xy/view.h>
#include <smeeze/views/details/xy/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
///////////////////////////////////////////////////////////////////////////////
template<typename container_type>
smeeze::views::details::xy::view<smeeze::views::details::xy::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>>
xy(container_type && c, size_t w, size_t h) {
  using iterator_type = smeeze::views::details::xy::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>;

  return smeeze::views::details::xy::view<iterator_type>(
    iterator_type(std::begin(c), 0,     true, w, 0, 0, w, h),
    iterator_type(std::begin(c), w * h, true, w, 0, 0, w, h),
    w,
    h
  );
}
///////////////////////////////////////////////////////////////////////////////
template<typename container_type>
auto
xy_transposed(container_type && c, size_t w, size_t h) {
  using iterator_type = smeeze::views::details::xy::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type>;

  return smeeze::views::details::xy::view<iterator_type>(
    iterator_type(std::begin(c), 0,     false, w, 0, 0, w, h),
    iterator_type(std::begin(c), w * h, false, w, 0, 0, w, h),
    h,
    w
  );
}
}
}

template<typename I>
inline
std::ostream &
operator<<(std::ostream & out, const smeeze::views::details::xy::view<I> & v) {
  const auto width(v.get_width());
  const auto height(v.get_height());
        auto b(std::begin(v));
  const auto e(std::end(v));

  for(size_t y=0;y<height;++y) {
    out << std::setw(3) << y << ":";
    for(size_t x=0;x<width;++x) {
      assert(b != e);
      out << " " << std::setw(6) << *b;
      ++b;
    }
    out << std::endl;
  }
  return out;
}
