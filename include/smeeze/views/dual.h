#pragma once

// system
#include <cassert>
// views
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/zip.h>

namespace smeeze {
namespace views {

template<typename iterator_type>
auto
dual(iterator_type && b, iterator_type && e) {
  assert(b != e);
  iterator_type n(std::next(std::forward<iterator_type>(b)));
  return smeeze::views::zip(
    smeeze::views::helpers::make_view(b, e),
    smeeze::views::helpers::make_view(n, e)
  );
}

template<typename container_type>
auto
dual(container_type && c) {
  return dual(std::begin(std::forward<container_type>(c)), std::end(std::forward<container_type>(c)));
}
}
}
