#pragma once

// system
#include <tuple>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/details/zip/iterator.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename... container_list>
auto
zip(container_list && ... c) {
  static_assert(sizeof...(container_list) > 1, "need more than one container");

  using iterator = smeeze::views::details::zip::iterator<typename smeeze::views::helpers::traits::get_container_iterator<container_list>::type...>;

  return smeeze::views::helpers::make_view(
    iterator(std::begin(c)...),
    iterator(std::end(c)...)
  );
}
}
}
