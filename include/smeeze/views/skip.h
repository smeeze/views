#pragma once

// system
#include <cassert>
#include <cstdint>
#include <iterator>
// smeeze
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/traits.h>

namespace smeeze {
namespace views {
template<typename container_type>
auto
skip(container_type && c, size_t amount) {
  assert(std::distance(std::begin(c), std::end(c)) >= (ssize_t)amount);

  using iterator_type = typename smeeze::views::helpers::traits::get_container_iterator<container_type>::type;

  return smeeze::views::helpers::make_view(
    iterator_type(std::next(std::begin(c), amount)),
    iterator_type(std::end(c))
  );
}
}
}
