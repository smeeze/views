// system
#include <iostream>
#include <vector>
// smeeze
#include <smeeze/views/iota.h>
#include <smeeze/views/rotate.h>
#include <smeeze/views/xy.h>
#include <smeeze/views/zip.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename T>
  class updater {
  public:
    updater(size_t width=6, size_t height=4, const T & n=0) : _width(width), _height(height), _data(_width * _height, n) {
    }

    auto operator*() const {
      return smeeze::views::xy(_data, _width, _height);
    }

    template<typename D>
    void set(size_t x, const D & new_data) {
      assert(new_data.get_height() <= _height);

      auto xy_view(smeeze::views::xy(_data, _width, _height));
      for(size_t y=0;y<new_data.get_height();++y) {
        for(auto z: smeeze::views::zip(smeeze::views::rotate(xy_view.get_row(y), x), new_data.get_row(y))) {
          std::get<0>(z) = std::get<1>(z);
        }
      }
    }
  private:
    size_t         _width;
    size_t         _height;
    std::vector<T> _data;
  };

  template<typename T>
  void test1() {
    updater<T> u(20);
    std::cout << *u << std::endl;

    {
      const size_t w(10);
      const size_t h(4);
      u.set(18, smeeze::views::xy(smeeze::views::iota<T>(w * h, 10), w, h));
      std::cout << *u << std::endl;
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test1<float>();
  return 0;
}
