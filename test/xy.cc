// system
#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>
// smeeze
#include <smeeze/views/helpers/print.h>
#include <smeeze/views/iota.h>
#include <smeeze/views/reverse.h>
#include <smeeze/views/rotate.h>
#include <smeeze/views/stride.h>
#include <smeeze/views/xy.h>
#include <smeeze/views/zip.h>
// test
#include <smeeze/test/render.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  static const size_t test_width(6);
  static const size_t test_height(4);
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_view() {
    SHOW_FUNCTION_HEADER;

    const auto v(smeeze::views::iota<T>(test_width * test_height, 10));
    const auto v_xy(smeeze::views::xy(v, test_width, test_height));

    std::cout << "v_xy:" << std::endl;
    std::cout << v_xy;

    std::cout << "v_xy.get_view(1, 1, 2, 2):" << std::endl;
    std::cout << v_xy.get_view(1, 1, 2, 2);
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_view_transposed() {
    SHOW_FUNCTION_HEADER;

    const auto v(smeeze::views::iota<T>(test_width * test_height, 10));
    const auto v_xy_transposed(smeeze::views::xy_transposed(v, test_width, test_height));

    std::cout << "v_xy_transposed:" << std::endl;
    std::cout << v_xy_transposed;

    std::cout << "v_xy_transposed.get_view(1, 1, 2, 2):" << std::endl;
    std::cout << v_xy_transposed.get_view(1, 1, 2, 2);

    std::cout << "v_xy_transposed.get_view_transposed():" << std::endl;
    std::cout << v_xy_transposed.get_view_transposed();

    std::cout << "v_xy_transposed.get_view(1, 1, 2, 2).get_view_transposed():" << std::endl;
    std::cout << v_xy_transposed.get_view(1, 1, 2, 2).get_view_transposed();
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_view_large() {
    SHOW_FUNCTION_HEADER;

    const auto i(smeeze::views::iota<T>(7 * 7, 10));
    auto v(std::vector<T>(std::begin(i), std::end(i)));
    auto v_full(smeeze::views::xy(v, 7, 7));
    auto v_sub(v_full.get_view(2, 2, 3, 3));

    // zero sub view
    for(auto & i: v_sub) { i = 0; }

    std::cout << "v_full:" << std::endl;
    std::cout << v_full;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_iota_on_view() {
    SHOW_FUNCTION_HEADER;

    const auto i(smeeze::views::iota<T>(3 * 3, 10));
    auto v(std::vector<T>(std::begin(i), std::end(i)));
    auto v_transposed(smeeze::views::xy_transposed(v, 3, 3));
    std::iota(std::begin(v_transposed), std::end(v_transposed), 10);

    std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_stride() {
    SHOW_FUNCTION_HEADER;

    auto v(std::vector<T>(4 * 4, 0));
    auto v_xy(smeeze::views::xy(v, 4, 4));
    auto v_stride(smeeze::views::stride(v_xy, 2));

    std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;

    std::iota(std::begin(v_stride), std::end(v_stride), 10);
    std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_row() {
    SHOW_FUNCTION_HEADER;

    auto v(std::vector<T>(5 * 5, 0));
    auto v_xy(smeeze::views::xy(v, 5, 5));
    auto r(v_xy.get_view(1, 1, 3, 1));
    for(auto & i: r) { i = 11; }

    std::cout << v_xy;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_column() {
    SHOW_FUNCTION_HEADER;

    auto v(std::vector<T>(5 * 5, 0));
    auto v_xy(smeeze::views::xy(v, 5, 5));
    auto r(v_xy.get_view(1, 1, 1, 3));
    for(auto & i: r) { i = 11; }

    std::cout << v_xy;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_misc() {
    SHOW_FUNCTION_HEADER;

    std::vector<T> v(9);
    auto v_transposed(smeeze::views::xy_transposed(v, 3, 3));
    std::iota(std::begin(v_transposed), std::end(v_transposed), 10);

    std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;

    smeeze::test::render::type(std::cout, v_transposed);
    smeeze::test::render::type(std::cout, std::begin(v_transposed));
    smeeze::test::render::type(std::cout, *std::begin(v_transposed));
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_sort() {
    SHOW_FUNCTION_HEADER;

    const auto i(smeeze::views::iota<T>(5 * 4, 10));
    auto vec(std::vector<T>(std::begin(i), std::end(i)));
    auto v(smeeze::views::xy(vec, 5, 4));
    auto v_t(v.get_view_transposed());

    std::cout << "before sort:" << std::endl << v;

    std::sort(std::begin(v_t), std::end(v_t));
    std::cout << "after sort:" << std::endl << v;
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_rotate() {
    SHOW_FUNCTION_HEADER;

    const auto vec(smeeze::views::iota<T>(5 * 4, 10));
    {
      const auto v(smeeze::views::xy(vec, 5, 4));
      std::cout << "v:" << std::endl << v << std::endl;
    }
    {
      const auto r(smeeze::views::rotate(vec, 1));
      const auto v(smeeze::views::xy(r, 5, 4));
      std::cout << "v:" << std::endl << v << std::endl;
    }
  }
///////////////////////////////////////////////////////////////////////////////
  template<typename T>
  void
  test_reverse() {
    SHOW_FUNCTION_HEADER;

    const auto data(smeeze::views::iota<T>(5 * 4, 10));
    const auto xy(smeeze::views::xy(data, 5, 4));

    {
      const auto row(xy.get_row(2));
      const auto row_reversed(smeeze::views::reverse(row));

      std::cout << "row........... " << smeeze::views::helpers::print(row) << std::endl;
      std::cout << "row_reversed.. " << smeeze::views::helpers::print(row_reversed) << std::endl;
    }

    {
      const auto column(xy.get_column(2));
      const auto column_reversed(smeeze::views::reverse(column));

      std::cout << "column........... " << smeeze::views::helpers::print(column) << std::endl;
      std::cout << "column_reversed.. " << smeeze::views::helpers::print(column_reversed) << std::endl;
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;

  test_view<int>();
  test_view_transposed<int>();
  test_view_large<int>();
  test_iota_on_view<int>();
  test_stride<int>();
  test_row<int>();
  test_column<int>();
  test_misc<int>();
  test_sort<int>();
  test_rotate<int>();
  test_reverse<int>();

  return 0;
}
