// system
#include <chrono>
#include <iostream>
#include <string>
// views
#include <smeeze/views/split.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  //
  //
  //
  const char * get_longish_string() {
    return "this is a longish string containing some words. and double  spaces. and triple   spaces";
  }
  //
  //
  //
  void test_string(const std::string & msg) {
    std::cout << __PRETTY_FUNCTION__ << "(" << msg << ")" << std::endl;
    for(auto && s: smeeze::views::split(msg, [](auto && c) { return c == ' '; })) {
      std::cout << "  s: " << std::string(std::begin(s), std::end(s)) << std::endl;
    }
  }
  //
  //
  //
  void test_replace(std::string msg) {
    std::cout << __PRETTY_FUNCTION__ << "(" << msg << ")" << std::endl;
    char n('a');
    for(auto && s: smeeze::views::split(msg, [](auto && c) { return c == ' '; })) {
      for(auto & i: s) {
        i = n;
      }
      ++n;
    }
    std::cout << __PRETTY_FUNCTION__ << "(" << msg << ")" << std::endl;
  }
  //
  //
  //
  void test_performance(std::string msg) {
    auto stamp(std::chrono::high_resolution_clock::now() + std::chrono::seconds(3));
    uint64_t n(0);
    while(std::chrono::high_resolution_clock::now() < stamp) {
      for(auto && s: smeeze::views::split(msg, [](auto && c) { return c == ' '; })) {
        for(auto & i: s) {
          i = '-';
        }
      }
      ++n;
    }

    std::cout << "n: " << n << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test_string("this is a test message");
  test_string("  start with a space");
  test_string("double  space");
  test_string("");
  test_string(" ");
  test_string("  ");
  test_string(get_longish_string());
  test_replace(get_longish_string());
  test_performance(get_longish_string());
  return 0;
}
