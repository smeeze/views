// system
#include <iostream>
// smeeze
#include <smeeze/views/iota.h>
#include <smeeze/views/reverse.h>
#include <smeeze/views/rotate.h>
#include <smeeze/views/helpers/print.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  auto iota(smeeze::views::iota<int>(10));
  // iota
  std::cout << "iota......... " << smeeze::views::helpers::print(iota) << std::endl;

  {
    auto reverse(smeeze::views::reverse(iota));
    std::cout << "  reverse.... " << smeeze::views::helpers::print(reverse) << std::endl;

    auto rotate(smeeze::views::rotate(reverse, 3));
    std::cout << "    rotate... " << smeeze::views::helpers::print(rotate) << std::endl;
  }

  {
    auto rotate(smeeze::views::rotate(iota, 3));
    std::cout << "  rotate..... " << smeeze::views::helpers::print(rotate) << std::endl;

    auto reverse(smeeze::views::reverse(rotate));
    std::cout << "    reverse.. " << smeeze::views::helpers::print(reverse) << std::endl;
  }

  return 0;
}
