// system
#include <iostream>
#include <numeric>
#include <vector>
// smeeze
#include <smeeze/views/rotate.h>
#include <smeeze/views/xy.h>
// test
#include <smeeze/views/helpers/print.h>
#include <smeeze/test/render.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename T>
  void test_rotate() {
    SHOW_FUNCTION_HEADER;

    std::vector<T> v(10);
    std::fill(std::begin(v), std::end(v), 0);

    std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;

    {
      auto v_rotate(smeeze::views::rotate(v, 2));
      std::iota(std::begin(v_rotate), std::end(v_rotate), 0);

      std::cout << "v:" << smeeze::views::helpers::print(v) << std::endl;
    }
  }

  template<typename T>
  void test_rotate_xy(int offset) {
    SHOW_FUNCTION_HEADER;

    std::vector<T> v(20);
    std::fill(std::begin(v), std::end(v), 0);

    auto v_xy(smeeze::views::xy(v, 5, 4));
    auto v_row(v_xy.get_row(1));
    {
      auto v_rotate(smeeze::views::rotate(v_row, offset));
      std::iota(std::begin(v_rotate), std::end(v_rotate), 0);
    }
    std::cout << v_xy;
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;

  test_rotate<int>();
  test_rotate_xy<int>(0);
  test_rotate_xy<int>(2);

  return 0;
}
