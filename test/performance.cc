// system
#include <cmath>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <functional>
#include <memory>
#include <string>
#include <vector>
// views
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/helpers/traits.h>
#include <smeeze/views/xy.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  //
  //
  //
  template<typename N>
  std::string bar(N value, N ref, const N lim = 40) {
    int l(lim);
    if(ref > 0) {
      l = lim * value / ref;
    }
    return std::string("|") + std::string(l, '-') + std::string("|");
  }
  //
  //
  //
  class basic_test {
  public:
             basic_test(const std::string & id) : _id(id) { }
    virtual ~basic_test() { }

    const std::string & get_id() const { return _id; }

    virtual void init() { }
    virtual void run(uint64_t n) = 0;
    virtual bool validate() const { return false; }
    virtual void fini() { }
  private:
    std::string _id;
  };
  //
  //
  //
  class test_manager {
  public:
    template<typename ... Ts>
    test_manager(Ts && ... ts) : _tests(ts...) { }

    void run(size_t seconds = 5) {
      try {
        std::cout.imbue(std::locale("en_US.UTF-8"));
      } catch(...) {
      }

      uint64_t ref(0);
      uint64_t sum(0);
      for(const auto & tst: _tests) {
        std::cout << "test:" << std::setw(32) << tst->get_id() << std::flush;
        auto loops(_run(tst, seconds));
        std::cout << ", loops: " << std::setw(12) << loops << " " << bar(loops, ref) << std::endl;
        sum += loops;
        if(ref==0) { // on first run, set ref
          ref = loops;
        }
      }
    }
  private:
    uint64_t _run(const std::shared_ptr<basic_test> & tst, size_t seconds) {
      uint64_t n(0);
      // init
      tst->init();
      // loop
      auto stamp_end(std::chrono::high_resolution_clock::now() + std::chrono::seconds(seconds));
      while(std::chrono::high_resolution_clock::now() < stamp_end) {
        tst->run(n);
        ++n;
      }
      // validate
      if(!tst->validate()) {
        throw(std::runtime_error(+ "failed validate for test " + tst->get_id()));
      }
      // fini
      tst->fini();
      // return
      return n;
    }
  private:
    std::vector<std::shared_ptr<basic_test>> _tests;
  };
  //
  //
  //
  class int_vector_test : public basic_test {
  public:
    int_vector_test(const std::string & id, int width, int height) : basic_test(id), _width(width), _height(height) {
      for(auto & i : _data) { i = 0; }
    }

    void init() override {
      _data.resize(_width * _height);
    }
    bool validate() const override {
      for(const auto & i : _data) {
        if(i != _last_loop_id) {
          return false;
        }
      }
      return true;
    }
    void fini() override {
      _data.resize(0);
    }
  protected:
    int              _width;
    int              _height;
    std::vector<int> _data;
    int              _last_loop_id;
  };
  //
  //
  //
  class raw_vector_iterate : public int_vector_test {
  public:
    using int_vector_test::int_vector_test;

    void run(uint64_t n) override {
      _last_loop_id = n;
      for(auto & i : _data) { i = n; }
    }
  };
  //
  //
  //
  class xy_view : public int_vector_test {
  public:
    using int_vector_test::int_vector_test;

    void run(uint64_t n) override {
      _last_loop_id = n;
      for(auto & i : smeeze::views::xy(_data, _width, _height)) { i = n; }
    }
  };
  //
  //
  //
  class xy_view_transposed : public int_vector_test {
  public:
    using int_vector_test::int_vector_test;

    void run(uint64_t n) override {
      _last_loop_id = n;
      for(auto & i : smeeze::views::xy_transposed(_data, _width, _height)) { i = n; }
    }
  };
  //
  //
  //
  class for_y_for_x : public int_vector_test {
  public:
    using int_vector_test::int_vector_test;

    void run(uint64_t n) override {
      _last_loop_id = n;
      for(int y=0;y<_height;++y) {
        for(int x=0;x<_width;++x) {
          _data[y * _width + x] = n;
        }
      }
    }
  };
  //
  //
  //
  class for_x_for_y : public int_vector_test {
  public:
    using int_vector_test::int_vector_test;

    void run(uint64_t n) override {
      _last_loop_id = n;
      for(int x=0;x<_width;++x) {
        for(int y=0;y<_height;++y) {
          _data[y * _width + x] = n;
        }
      }
    }
  };
  //
  //
  //
  void run_test_xy_views(const size_t & width, const size_t & height) {
    std::cout << __PRETTY_FUNCTION__ << "(" << width << ", " << height << ")" << std::endl;

    test_manager tm(std::vector<std::shared_ptr<basic_test>>{
      std::make_shared<raw_vector_iterate>("raw vector iterator", width, height),
      std::make_shared<xy_view>("xy view", width, height),
      std::make_shared<xy_view_transposed>("xy view transposed", width, height),
      std::make_shared<for_y_for_x>("for(y), for(x)", width, height),
      std::make_shared<for_x_for_y>("for(x), for(y)", width, height),
    });

    tm.run();
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    run_test_xy_views(10,    10);
    run_test_xy_views(100,   100);
    run_test_xy_views(10000, 64);
    run_test_xy_views(64,    10000);
    run_test_xy_views(10000, 10000);
  } catch(const std::exception & e) {
    std::cerr << "error: " << e.what() << std::endl;
  }
  return 0;
}
