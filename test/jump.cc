// system
#include <iostream>
#include <string>
#include <vector>
// smeeze
#include <smeeze/views/jump.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;

  const std::vector<std::string> v_data{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
  const std::vector<int>         v_offset{1, 3, 0, 6};

  std::cout << "v_data:";
  for(const auto & v: v_data) { std::cout << " " << v; }
  std::cout << std::endl;

  std::cout << "v_offset:";
  for(const auto & v: v_offset) { std::cout << " " << v; }
  std::cout << std::endl;

  std::cout << "jt:";
  for(const auto & v: smeeze::views::details::jump::table(v_offset)) { std::cout << " " << v; }
  std::cout << std::endl;

  std::cout << "v_jump:";
  for(const auto & v: smeeze::views::jump(v_data, v_offset)) { std::cout << " " << v; }
  std::cout << std::endl;

  {
    std::cout << "jt:";
    for(const auto & v: smeeze::views::details::jump::table(std::vector<int>{0, 2, 3, 6, 9})) { std::cout << " " << v; }
    std::cout << std::endl;
  }
  {
    std::cout << "jt:";
    for(const auto & v: smeeze::views::details::jump::table(std::vector<int>{0, 4, 2, 9, 1})) { std::cout << " " << v; }
    std::cout << std::endl;
  }
  {
    std::cout << "jt:";
    for(const auto & v: smeeze::views::details::jump::table(std::vector<int>{5, 6, 7, 0, 1, 2})) { std::cout << " " << v; }
    std::cout << std::endl;
  }

  return 0;
}
