// system
#include <chrono>
#include <iostream>
#include <string>
// views
#include <smeeze/views/strip.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  //
  //
  //
  const char * get_longish_string() {
    return "this is a longish string containing some words. and double  spaces. and triple   spaces.";
  }
  //
  //
  //
  void test(const std::string & msg) {
    auto strip(smeeze::views::strip(msg, [](const auto & c) { return c == ' '; }));
    std::string s(std::begin(strip), std::end(strip));
    std::cout << "msg: " << msg << std::endl;
    std::cout << "s: " << s << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test(get_longish_string());
  return 0;
}
