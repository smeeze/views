// system
#include <iostream>
#include <string>
#include <vector>
// smeeze
#include <smeeze/views/join.h>
#include <smeeze/views/reverse.h>
#include <smeeze/views/split.h>
///////////////////////////////////////////////////////////////////////////////
#define TEST_JOIN_V1
///////////////////////////////////////////////////////////////////////////////
namespace {
#ifdef TEST_JOIN_V1
  void test_join(const std::vector<std::string> & v) {
    auto j(smeeze::views::join(v));
    std::cout << "j: " << std::string(std::begin(j), std::end(j)) << std::endl;
  }
  void test_join(const std::string & s1, const std::string & s2) {
    test_join(std::vector<std::string>{s1, s2});
  }

  void test_split_join(const std::string & msg) {
    std::cout << "msg: " << msg << std::endl;

    for(const auto & split_char: std::string(" ")) {
      std::cout << "split char: '" << split_char << "'" << std::endl;

      auto split(smeeze::views::split(msg, [&split_char](const auto & c) { return c == split_char; }));
      std::cout << "  split:";
      for(const auto & s: split) {
        std::cout << "[" << std::string(std::begin(s), std::end(s)) << "]";
      }
      std::cout << std::endl;

      auto join(smeeze::views::join(split));
      std::cout << "  join: " << std::string(std::begin(join), std::end(join)) << std::endl;
    }
  }
#else
  void test1() {
    const std::string msg("this is a sentence");

    const char split_char(' ');
    const auto split(smeeze::views::split(msg, [&split_char](const auto & c) { return c == split_char; }));
    std::cout << "  split:";
    for(const auto & s: split) {
      std::cout << "[" << std::string(std::begin(s), std::end(s)) << "]";
    }
    std::cout << std::endl;

    const auto join(smeeze::views::join(split));
    std::cout << "  join: " << std::string(std::begin(join), std::end(join)) << std::endl;

    const auto reverse(smeeze::views::reverse(join));
    std::cout << "  reverse:";
    for(const auto & s: reverse) {
      std::cout << "[" << std::string(std::begin(s), std::end(s)) << "]";
    }
    std::cout << std::endl;
  }
#endif
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;
#ifdef TEST_JOIN_V1
  test_join("[line 1]", "[line 2]");
  test_split_join("this is a line with multiple    spaces and stuff");
#else
  test1();
#endif
  return 0;
}
