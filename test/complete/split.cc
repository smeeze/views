#include <algorithm>
#include <iostream>
#include <vector>

namespace {
  //
  //
  //
  template<typename T>
  struct get_container_iterator {
    using type =
      typename std::remove_reference<
        decltype(
          std::begin(
            std::declval<T>()
          )
        )
      >::type
    ;
  };
  //
  //
  //
  template<typename iterator_type>
  class generic_view {
  public:
    generic_view(const iterator_type & b, const iterator_type & e) : _begin(b), _end(e) {
    }

    const iterator_type & cbegin() const { return _begin; }
    const iterator_type & cend()   const { return _end; }

    const iterator_type & begin() const { return _begin; }
    const iterator_type & end()   const { return _end; }

    iterator_type & begin() { return _begin; }
    iterator_type & end()   { return _end; }
  private:
    iterator_type _begin;
    iterator_type _end;
  };
  //
  //
  //
  template<typename I, typename P>
  class iterator : public std::iterator<std::forward_iterator_tag, generic_view<I>> {
  public:
    iterator(const I & b, const I & e, const P & p) : _view(b, b), _end(e), _unary_predicate(p) {
      _find_next_state();
    }
    iterator(const I & b, const P & p) : _view(b, b), _unary_predicate(p) {
    }

    // prefix
    iterator & operator++() {
      _find_next_state();
      return *this;
    }

    // compare
    bool operator==(const iterator & rhs) const { return _view.begin() == rhs._view.begin(); }
    bool operator!=(const iterator & rhs) const { return _view.begin() != rhs._view.begin(); }

    // dereference
    const auto & operator*() const { return _view; }
          auto & operator*()       { return _view; }
  private:
    void _find_next_state() {
      auto b(std::find_if_not(_view.end(), _end, _unary_predicate));
      auto e(std::find_if    (b,           _end, _unary_predicate));
      _view = generic_view<I>(b, e);
    }
  private:
    generic_view<I> _view;
    I _end;
    P _unary_predicate;
  };
  //
  //
  //
  template<typename container_type, typename unary_predicate>
  auto
  split(container_type && c, unary_predicate && p) {
    using iterator_type = iterator<typename get_container_iterator<container_type>::type, unary_predicate>;

    auto b(std::begin(c));
    auto e(std::end(c));
    return generic_view<iterator_type>(
      iterator_type(b, e, std::forward<unary_predicate>(p)),
      iterator_type(e, std::forward<unary_predicate>(p))
    );
  }
}

int main(int, char **) {
  std::vector<int> v{1, 2, 3, 0, 4, 5, 6};
  for(const auto & s: split(v, [](const int & i) -> bool { return i == 0; })) {
    std::cout << "s:";
    for(const auto & i: s) {
      std::cout << " " << i;
    }
    std::cout << std::endl;
  }
  return 0;
}
