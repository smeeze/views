// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/skip.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-skip") {
  std::vector<int> v1_int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  std::vector<int> v2_int{0, 0, 0, 1, 2, 3, 4, 5, 6, 7};

  auto skip(smeeze::views::skip(v1_int, 2));
  std::iota(std::begin(skip), std::end(skip), 0);

  smeeze::test::compare::container(v1_int, v2_int);
}
