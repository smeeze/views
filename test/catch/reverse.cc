// system
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/iota.h>
#include <smeeze/views/reverse.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-reverse") {
  const std::vector<int> test_data{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const std::vector<int> check_data{9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

  const auto reverse(smeeze::views::reverse(test_data));

  smeeze::test::compare::container(reverse, check_data);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-reverse-iota") {
  const auto test_data(smeeze::views::iota<int>(10));
  const std::vector<int> check_data{9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

  const auto reverse(smeeze::views::reverse(test_data));

  smeeze::test::compare::container(reverse, check_data);
}
