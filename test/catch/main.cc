// system
#include <vector>
// catch
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-main") {
  std::vector<int> v1{0, 1, 2, 3};
  std::vector<int> v2{0, 1, 2, 3};

  smeeze::test::compare::container(v1, v2);
}
