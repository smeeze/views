// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/helpers/traits.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-traits") {
  const std::vector<int> v1{0, 1, 2, 3, 4};
  const auto before_end(smeeze::views::helpers::traits::before_end_iterator(v1));
  REQUIRE(*before_end == 4);
}
