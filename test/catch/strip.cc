// system
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/strip.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-strip") {
  const std::vector<int> v{1, 2, 3, 0, 4, 5, 0, 6, 7, 8, 0, 9};
  const auto strip(smeeze::views::strip(v, [](const auto & i) { return i == 0; }));
  const std::vector<int> v_strip(std::begin(strip), std::end(strip));

  smeeze::test::compare::container(v_strip, std::vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9});
}
