// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/stride.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-stride") {
  std::vector<int> v1_int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  std::vector<int> v2_int{0, 0, 1, 0, 2, 0, 3, 0, 4, 0};

  auto stride(smeeze::views::stride(v1_int, 2));
  std::iota(std::begin(stride), std::end(stride), 0);

  smeeze::test::compare::container(v1_int, v2_int);
}
