// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/join.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-join") {
  const std::vector<std::vector<int>> v{{0, 1, 2, 3, 4}, {5, 6, 7, 8, 9}};
  const std::vector<int> v_check{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  auto join(smeeze::views::join(v));

  smeeze::test::compare::container(join, v_check);
}
