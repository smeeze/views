// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/jump.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-jump-table") {
  //                                        0    1    2    3    4    5    6    7    8    9
  const std::vector<std::string> test_data{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"};
  SECTION("start at zero") {
    const std::vector<int> indices{0, 2, 3, 6, 9};
    smeeze::views::details::jump::table jt(indices);
    {
      const std::vector<int> jt_check{0, 2, 1, 3, 3};
      smeeze::test::compare::container(jt, jt_check);
    }

    smeeze::test::compare::container(smeeze::views::jump(test_data, jt), std::vector<std::string>{"a", "c", "d", "g", "j"});
  }
  SECTION("go back") {
    const std::vector<int> indices{0, 4, 2, 9, 1};
    smeeze::views::details::jump::table jt(indices);
    {
      const std::vector<int> jt_check{0, 4, -2, 7, -8};
      smeeze::test::compare::container(jt, jt_check);
    }

    smeeze::test::compare::container(smeeze::views::jump(test_data, jt), std::vector<std::string>{"a", "e", "c", "j", "b"});
  }
  SECTION("start at other than zero") {
    const std::vector<int> indices{5, 6, 7, 0, 1, 2};
    smeeze::views::details::jump::table jt(indices);
    {
      const std::vector<int> jt_check{5, 1, 1, -7, 1, 1};
      smeeze::test::compare::container(jt, jt_check);
    }

    smeeze::test::compare::container(smeeze::views::jump(test_data, jt), std::vector<std::string>{"f", "g", "h", "a", "b", "c"});
  }
}
