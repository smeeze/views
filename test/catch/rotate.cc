// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/rotate.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-rotate") {
  const std::vector<int> v1_int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const std::vector<int> v2_int{8, 9, 0, 1, 2, 3, 4, 5, 6, 7};

  SECTION("forward") {
    auto rotate(smeeze::views::rotate(v2_int, 2));
    smeeze::test::compare::container(v1_int, rotate);
  }

  SECTION("backward, 8") {
    auto rotate(smeeze::views::rotate(v1_int, 8));
    smeeze::test::compare::container(rotate, v2_int);
  }
}
