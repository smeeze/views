// system
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/dual.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-dual") {
  std::vector<int> vi{0, 1, 2, 3, 4, 5};

  for(auto && z: smeeze::views::dual(vi)) {
    REQUIRE( (std::get<1>(z) - std::get<0>(z)) == 1);
  }
}
