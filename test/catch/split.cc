// system
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/split.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename T>
  std::vector<std::vector<T>> v_split(const std::vector<T> & v) {
    std::vector<std::vector<T>> rv;
    for(auto i: smeeze::views::split(v, [](const auto & t) { return t == 0; })) {
      rv.push_back(std::vector<int>(std::begin(i), std::end(i)));
    }
    return rv;
  }
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-split-multi") {
  auto vv(v_split<int>(std::vector<int>{1, 2, 3, 0, 4, 5, 0, 6, 7, 0, 0, 8, 9}));
  REQUIRE(vv.size() == 4);
  smeeze::test::compare::container(vv[0], std::vector<int>{1, 2, 3});
  smeeze::test::compare::container(vv[1], std::vector<int>{4, 5});
  smeeze::test::compare::container(vv[2], std::vector<int>{6, 7});
  smeeze::test::compare::container(vv[3], std::vector<int>{8, 9});
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-split-left-pad") {
  auto vv(v_split<int>(std::vector<int>{0, 1, 2}));
  REQUIRE(vv.size() == 1);
  smeeze::test::compare::container(vv[0], std::vector<int>{1, 2});
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-split-both-pad") {
  auto vv(v_split<int>(std::vector<int>{0, 1, 2, 0}));
  REQUIRE(vv.size() == 1);
  smeeze::test::compare::container(vv[0], std::vector<int>{1, 2});
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-split-empty") {
  SECTION("empty") {
    auto vv(v_split<int>(std::vector<int>{}));
    REQUIRE(vv.size() == 0);
  }
  SECTION("one zero") {
    auto vv(v_split<int>(std::vector<int>{0}));
    REQUIRE(vv.size() == 0);
  }
  SECTION("two zero") {
    auto vv(v_split<int>(std::vector<int>{0, 0}));
    REQUIRE(vv.size() == 0);
  }
}
