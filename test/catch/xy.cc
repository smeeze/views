// system
#include <array>
#include <numeric>
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/xy.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename T>
  std::vector<T> get_zero_vector() {
  }
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-xy") {
  const std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7, 8};

  auto v_xy(smeeze::views::xy(v, 3, 3));
  smeeze::test::compare::container(v, v_xy);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-xy-transposed") {
  const std::vector<int> v_storage{0, 1, 2, 3, 4, 5, 6, 7, 8};
  const std::vector<int> v_check{0, 3, 6, 1, 4, 7, 2, 5, 8};

  auto v_xy_transposed(smeeze::views::xy_transposed(v_storage, 3, 3));
  smeeze::test::compare::container(v_check, v_xy_transposed);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-xy-iota-on-view") {
        std::vector<int> v_storage{0, 0, 0, 0, 0, 0, 0, 0, 0};
  const std::vector<int> v_check{0, 3, 6, 1, 4, 7, 2, 5, 8};

  auto v_xy_transposed(smeeze::views::xy_transposed(v_storage, 3, 3));
  std::iota(std::begin(v_xy_transposed), std::end(v_xy_transposed), 0);

  smeeze::test::compare::container(v_storage, v_check);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-xy-array") {
  const std::array<int, 9> v{{0, 1, 2, 3, 4, 5, 6, 7, 8}};

  const auto v_xy(smeeze::views::xy(v, 3, 3));
  smeeze::test::compare::container(v, v_xy);

  SECTION("row0") {
    const auto row(v_xy.get_row(0));
    smeeze::test::compare::container(row, std::vector<int>({ {0, 1, 2} }));
  }
  SECTION("row1") {
    const auto row(v_xy.get_row(1));
    smeeze::test::compare::container(row, std::vector<int>({ { 3, 4, 5 } }));
  }
  SECTION("column0") {
    const auto column(v_xy.get_column(0));
    smeeze::test::compare::container(column, std::vector<int>({ { 0, 3, 6 } }));
  }
  SECTION("column1") {
    const auto column(v_xy.get_column(1));
    smeeze::test::compare::container(column, std::vector<int>({ { 1, 4, 7 } }));
  }
}
