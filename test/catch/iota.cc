// system
#include <vector>
// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/iota.h>
#include <smeeze/views/reverse.h>
#include <smeeze/views/rotate.h>
// test
#include <smeeze/test/compare.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-test-iota") {
  const std::vector<int> test_data{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  const auto iota(smeeze::views::iota<int>(10));
  smeeze::test::compare::container(test_data, iota);

  SECTION("reverse") {
    const std::vector<int> reverse_test_data{9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    const auto reverse(smeeze::views::reverse(iota));
    smeeze::test::compare::container(reverse_test_data, reverse);

    SECTION("rotate") {
      const std::vector<int> rotate_test_data{6, 5, 4, 3, 2, 1, 0, 9, 8, 7};
      const auto rotate(smeeze::views::rotate(reverse, 3));
      smeeze::test::compare::container(rotate_test_data, rotate);
    }
  }

  SECTION("rotate") {
    const std::vector<int> rotate_test_data{3, 4, 5, 6, 7, 8, 9, 0, 1, 2};
    const auto rotate(smeeze::views::rotate(iota, 3));
    smeeze::test::compare::container(rotate_test_data, rotate);

    SECTION("reverse") {
      const std::vector<int> reverse_test_data{2, 1, 0, 9, 8, 7, 6, 5, 4, 3};
      const auto reverse(smeeze::views::reverse(rotate));
      smeeze::test::compare::container(reverse_test_data, reverse);
    }
  }
}
