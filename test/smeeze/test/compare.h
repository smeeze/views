#pragma once

// catch
#include <catch.hpp>
// smeeze
#include <smeeze/views/zip.h>

namespace smeeze {
namespace test {
namespace compare {
template<typename container1_type, typename container2_type>
void
container(container1_type && c1, container2_type && c2) {
  REQUIRE((std::distance(std::begin(c1), std::end(c1)) == std::distance(std::begin(c2), std::end(c2))));

  for(auto z: smeeze::views::zip(c1, c2)) {
    REQUIRE((std::get<0>(z) == std::get<1>(z)));
  }
}
}
}
}
