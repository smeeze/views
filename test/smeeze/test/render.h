#pragma once

// system
#include <iomanip>
#include <ostream>

#define SHOW_FUNCTION_HEADER { \
  std::cout << "-------------------------------------------------------------------------------" << std::endl; \
  std::cout << __PRETTY_FUNCTION__ << std::endl; \
}

namespace smeeze {
namespace test {
namespace render {
///////////////////////////////////////////////////////////////////////////////
template<typename T>
void
type(std::ostream & out, T &&) {
  out << __PRETTY_FUNCTION__ << std::endl;
}
}
}
}
