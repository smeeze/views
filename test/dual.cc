// system
#include <iostream>
#include <vector>
// views
#include <smeeze/views/dual.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  void test_dual_iter() {
    std::vector<int> vi{{0, 1, 2, 3, 4, 5}};

    for(auto && z: smeeze::views::dual(vi)) {
      std::cout << "z: " << std::get<0>(z) << ", " << std::get<1>(z) << std::endl;
    }

    for(auto && z: smeeze::views::dual(std::begin(vi), std::end(vi))) {
      std::cout << "z: " << std::get<0>(z) << ", " << std::get<1>(z) << std::endl;
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test_dual_iter();
  return 0;
}
