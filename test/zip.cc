// system
#include <iostream>
#include <numeric>
#include <vector>
// smeeze
#include <smeeze/views/helpers/print.h>
#include <smeeze/views/iota.h>
#include <smeeze/views/skip.h>
#include <smeeze/views/zip.h>
// test
#include <smeeze/test/render.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename T>
  void
  test_long_short() {
    SHOW_FUNCTION_HEADER;

    const auto v(smeeze::views::iota<T>(10));
    auto skip(smeeze::views::skip(v, 3));
    auto zip(smeeze::views::zip(v, skip));

    std::cout << "zip:";
    for(const auto & z: zip) {
      std::cout << "[" << std::get<0>(z) << " " << std::get<1>(z) << "]";
    }
    std::cout << std::endl;
  }

  template<typename T>
  void
  test_misc() {
    SHOW_FUNCTION_HEADER;

    std::vector<T> v1(10);
    std::vector<T> v2(10);

    std::iota(std::begin(v1), std::end(v1), 0);

    std::cout << "v1:" << smeeze::views::helpers::print(v1) << std::endl;
    std::cout << "v2:" << smeeze::views::helpers::print(v2) << std::endl;

    {
      for(auto i: smeeze::views::zip(v1, v2)) {
        std::swap(
          std::get<0>(i),
          std::get<1>(i)
        );
      }
    }

    std::cout << "v1:" << smeeze::views::helpers::print(v1) << std::endl;
    std::cout << "v2:" << smeeze::views::helpers::print(v2) << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;

  test_long_short<int>();
  test_misc<int>();

  return 0;
}
