// system
#include <iostream>
#include <vector>
// smeeze
#include <smeeze/views/helpers/print.h>
#include <smeeze/views/reverse.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  void test1() {
    std::vector<int> v{0, 1, 2, 3, 4, 5};
    auto r(smeeze::views::reverse(v));
    std::cout << "v: " << smeeze::views::helpers::print(v) << std::endl;
    std::cout << "r: " << smeeze::views::helpers::print(r) << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test1();
  return 0;
}
