// system
#include <iostream>
#include <numeric>
#include <vector>
// smeeze
#include <smeeze/views/stride.h>
#include <smeeze/views/helpers/print.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int, char ** argv) {
  std::cout << "--- " << argv[0] << " ---" << std::endl;

  std::vector<int> v_int(10);

  std::cout << "v_int:" << smeeze::views::helpers::print(v_int) << std::endl;

  {
    auto v_stride(smeeze::views::stride(v_int, 2));
    std::iota(std::begin(v_stride), std::end(v_stride), 10);

    std::cout << "v_stride:" << smeeze::views::helpers::print(v_stride) << std::endl;
  }

  std::cout << "v_int:" << smeeze::views::helpers::print(v_int) << std::endl;

  return 0;
}
