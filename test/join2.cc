// system
#include <iostream>
#include <string>
#include <list>
#include <vector>
// smeeze
#include <smeeze/views/join2.h>
#include <smeeze/views/helpers/print.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  void test1() {
    std::vector<int> v{0, 1, 2, 3, 4};
    std::list<int>   l{5, 6, 7, 8, 9};

    auto join2(smeeze::views::join2(v, l));
    std::cout << "join2: " << smeeze::views::helpers::print(join2) << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  test1();
  return 0;
}
