find_package(Git REQUIRED)

include(ExternalProject)

ExternalProject_Add(
  external_catch
  GIT_REPOSITORY https://github.com/philsquared/Catch.git
  UPDATE_COMMAND ""
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
)

ExternalProject_Get_Property(external_catch source_dir)
set(CATCH_INCLUDE_DIR ${source_dir}/single_include)
message("CATCH_INCLUDE_DIR......... " ${CATCH_INCLUDE_DIR})
