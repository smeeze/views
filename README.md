# views
collection of views for c++

## xy view
allows you to iterate in two dimensions.
for example:

in c++:
```C++
std::vector<int> v_int(9);
auto v_xy(smeeze::views::xy_transposed(v_int, 3, 3));
std::iota(v_xy.begin(), v_xy.end(), 10);
```
will fill the vector with the following elements: `[ 10 13 16 11 14 17 12 15 18 ]`
