cmake_minimum_required(VERSION 3.4)

project(smeeze-views)

include(cmake/gcc.cmake)
include(cmake/clang.cmake)
include(cmake/msvc.cmake)
include(cmake/ccache.cmake)
include(cmake/catch.cmake)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

foreach(f iota join join2 jump performance reverse rotate skip split stride strip xy zip dual)
  add_executable(test-${f} test/${f}.cc)
  target_include_directories(test-${f} PRIVATE include PRIVATE test)
endforeach()

add_executable(test-misc-2d-update test/misc/2d-update.cc)
target_include_directories(test-misc-2d-update PRIVATE include PRIVATE test)

###################
# catch unit test #
###################
add_executable(test-catch
  test/catch/dual.cc
  test/catch/iota.cc
  test/catch/join2.cc
  test/catch/join.cc
  test/catch/jump.cc
  test/catch/main.cc
  test/catch/reverse.cc
  test/catch/rotate.cc
  test/catch/skip.cc
  test/catch/split.cc
  test/catch/stride.cc
  test/catch/strip.cc
  test/catch/traits.cc
  test/catch/xy.cc
  test/catch/zip.cc
)
target_include_directories(test-catch PRIVATE include PRIVATE test)
target_include_directories(test-catch SYSTEM PRIVATE ${CATCH_INCLUDE_DIR})
add_dependencies(test-catch external_catch)
# add unit test to ALL target
add_custom_target(test-catch-all ALL COMMAND test-catch)
